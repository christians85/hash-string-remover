This simple tool takes two Strings by command line. Then it will remove all Characters of the second input out of the first.

Example:
========

Execution
-----
java -jar RemoveCharsOfBFromA.jar "ThiAs seAntAense coAntains toAo maAny upApercase aAAAAA." "A"

Result:
-------
Input A:                ThiAs seAntAense coAntains toAo maAny upApercase aAAAAA.  
Input B:                A  
A without Chars of B:   This sentense contains too many uppercase a.  
import java.util.HashMap;

public class DeleteBFromA {
    public static void main(String[] args) {

        // Checks if two Strings were given as params.
        if(args.length != 2)  {
            System.out.println("Please enter two strings by command line.");
            return;
        }

        // Get both Strings from args-Array.
        String a = args[0];
        String b = args[1];
        String result = new String();

        /*
         * This Hashmap will be filled with all Characters
         * in Array B. The Advantage is that otherwise for
         * every Character in A the whole B Array would have
         * to be iterated to find if it's inside of B.
         * The Hashmap allows check this much more efficient.
         */
        HashMap<Character, Integer> map = new HashMap<>();
        for(int i = 0; i < b.length(); i++) {
            map.put(b.charAt(i), 1);
        }

        /*
         * This loop iterates over all Characters inside of
         * Array A. If the HashMap does not contain the Character
         * as key, it wasn't part of B, so it will be added to
         * a result String. Otherwise B contains this Character
         * and the look continues with the next Character of A,
         * so the B-contained Chacacter won't be in the result.
         */
        for(int i = 0; i < a.length(); i++) {
            if (map.containsKey(a.charAt(i)))
                continue;
            result += a.charAt(i);
        }

        System.out.println("Input A: \t\t" + a);
        System.out.println("Input B: \t\t" + b);
        // Makes A the result => A doesn't contain Characters of B anymore.

        a = result;
        System.out.println("A without Chars of B: \t" + a);
    }
}